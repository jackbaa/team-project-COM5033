package com.example.pizzatimev4.Model;

public class Model {

    String fullName, cityName, profilepic, pinCode, phoneNumber, address,userId;
    String Location, imageUrl, itemName, itemPrice;
    public Model() {
    }
    public Model(String fullName,String cityName, String profilepic, String pinCode, String phoneNumber, String address, String userId, String Location, String imageUrl, String itemName, String itemPrice) {
        this.fullName = fullName;
        this.cityName = cityName;
        this.profilepic = profilepic;
        this.pinCode = pinCode;
        this.phoneNumber = phoneNumber;
        this.address = address;
        this.userId = userId;
        this.Location = Location;
        this.imageUrl = imageUrl;
        this.itemName = itemName;
        this.itemPrice = itemPrice;
    }
    public String getName() {
        return fullName;
    }
    public void setName(String fullName) {
        this.fullName = fullName;
    }
    public String getCityName() {
        return cityName;
    }
    public void setCityName(String cityName) {
        this.cityName = cityName;
    }
    public String getPinCode() {
        return pinCode;
    }
    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }
    public String getPhoneNumber() {
        return phoneNumber;
    }
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }
    public String getLocation() {
        return Location;
    }
    public String getImageUrl() {
        return imageUrl;
    }
    public String getItemName() {
        return itemName;
    }
    public String getItemPrice() {
        return itemPrice;
    }

}