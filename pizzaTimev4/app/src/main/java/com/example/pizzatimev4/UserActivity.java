package com.example.pizzatimev4;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.example.pizzatimev4.Fragments.HomeFragment;
import com.example.pizzatimev4.Fragments.MyOrdersFragment;
import com.example.pizzatimev4.Fragments.UserProfileFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class UserActivity extends AppCompatActivity {
    FrameLayout frameLayout;
    BottomNavigationView bottomNavigationView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        frameLayout = (FrameLayout) findViewById(R.id.UserFragmentContainer);
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.UserBottomNavigationBar);
        Menu menuNav = bottomNavigationView.getMenu();
        getSupportFragmentManager().beginTransaction().replace(R.id.UserFragmentContainer, new HomeFragment()).commit();
        bottomNavigationView.setOnNavigationItemSelectedListener(bottomNavigationMethod);
    }
    private BottomNavigationView.OnNavigationItemSelectedListener bottomNavigationMethod =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull @org.jetbrains.annotations.NotNull MenuItem item) {

                    Fragment fragment = null;
                    switch (item.getItemId()) {

                        case R.id.HomeMenu:
                            fragment = new HomeFragment();
                            break;
                        case R.id.MyOrdersMenu:
                            fragment = new MyOrdersFragment();
                            break;

                        case R.id.ProfileMenu:
                            fragment = new UserProfileFragment();
                            break;
                    }
                    getSupportFragmentManager().beginTransaction().replace(R.id.UserFragmentContainer, fragment).commit();
                    return true;
                }
            };
}